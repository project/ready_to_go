; Drupal.org release file.
core = 7.x
api = 2

; Basic contributed modules.
projects[ctools][subdir] = "contrib"
projects[ctools][version] = 1.4

projects[entity][subdir] = "contrib"
projects[entity][version] = 1.5

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = 1.1
projects[entityreference][patch][] = "https://www.drupal.org/files/1580348-universal-formatters-17.patch"

projects[rules][subdir] = "contrib"
projects[rules][version] = 2.7

projects[views][subdir] = "contrib"
projects[views][version] = 3.8

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = 3.2

projects[addressfield][subdir] = "contrib"
projects[addressfield][version] = 1.0-beta5

projects[field_group][subdir] = "contrib"
projects[field_group][version] = 1.4

projects[linkit][subdir] = "contrib"
projects[linkit][version] = 3.1

projects[linkit_picker][subdir] = "contrib"
projects[linkit_picker][version] = 7.x-3.0-rc2

projects[features][subdir] = "contrib"
projects[features][version] = 2.0

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = 2.0

projects[taxonomy_menu][subdir] = "contrib"
projects[taxonomy_menu][version] = 1.5

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = 2.4

projects[libraries][subdir] = "contrib"
projects[libraries][version] = 2.2

projects[views_megarow][subdir] = "contrib"
projects[views_megarow][version] = 1.3

projects[panels][subdir] = "contrib"
projects[panels][version] = 3.4

projects[panelizer][subdir] = contrib
projects[panelizer][version] = 3.1

projects[edit][subdir] = "contrib"
projects[edit][version] = 1.0

projects[ckeditor][subdir] = contrib
projects[ckeditor][version] = 1.15

projects[ckeditor_link][subdir] = contrib
projects[ckeditor_link][version] = 2.3

projects[layout][subdir] = contrib
projects[layout][version] = 1.0-alpha6

; Other contribs.
projects[bg_image][subdir] = "contrib"
projects[bg_image][version] = 1.4

projects[nodequeue][subdir] = "contrib"
projects[nodequeue][version] = 2.0-beta1

projects[entityform][subdir] = "contrib"
projects[entityform][version] = 2.0-beta4

projects[draggable_captcha][subdir] = "contrib"
projects[draggable_captcha][version] = 1.2

projects[media][subdir] = "contrib"
projects[media][version] = 1.4

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = 2.0-alpha3

projects[ds][subdir] = "contrib"
projects[ds][version] = 2.6

projects[devel][subdir] = "contrib"
projects[devel][version] = 1.5

projects[devel_themer][subdir] = "contrib"
projects[devel_themer][version] = 1.x-dev

projects[diff][subdir] = "contrib"
projects[diff][version] = 3.2

projects[bean][subdir] = "contrib"
projects[bean][version] = 1.7

projects[countries][subdir] = "contrib"
projects[countries][version] = 2.1

projects[remote_stream_wrapper][subdir] = "contrib"
projects[remote_stream_wrapper][version] = 1.0-rc1

projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = 2.7

projects[crumbs][subdir] = "contrib"
projects[crumbs][version] = 2.0-beta15

projects[http_client][subdir] = "contrib"
projects[http_client][version] = 2.4

projects[oauth][subdir] = "contrib"
projects[oauth][version] = 3.2
projects[oauth][patch][] = "https://drupal.org/files/980340-d7.patch"

projects[connector][subdir] = "contrib"
projects[connector][version] = 1.0-beta2

projects[oauthconnector][subdir] = "contrib"
projects[oauthconnector][version] = 1.0-beta2

projects[inline_entity_form][subdir] = "contrib"
projects[inline_entity_form][version] = 1.5

projects[inline_conditions][subdir] = "contrib"
projects[inline_conditions][version] = 1.0-alpha4

projects[field_extractor][subdir] = "contrib"
projects[field_extractor][version] = 1.3

projects[service_links][subdir] = "contrib"
projects[service_links][version] = 2.2
projects[service_links][patch][] = "https://drupal.org/files/drupal7-service_links-2.2-update_1.patch"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = 1.1

projects[examples][subdir] = "contrib"
projects[examples][version] = 1.x-dev

projects[mailsystem][subdir] = "contrib"
projects[mailsystem][version] = 2.34

projects[mimemail][subdir] = "contrib"
projects[mimemail][version] = 1.0-beta3

projects[email][subdir] = "contrib"
projects[email][version] = 1.3

projects[token][subdir] = "contrib"
projects[token][version] = 1.5
projects[token][patch][] = "https://drupal.org/files/token-token_asort_tokens-1712336_0.patch"

projects[eva][subdir] = "contrib"
projects[eva][version] = 1.2

projects[message][subdir] = "contrib"
projects[message][version] = 1.9

projects[message_notify][subdir] = "contrib"
projects[message_notify][version] = 2.5

projects[migrate][subdir] = "contrib"
projects[migrate][version] = 2.5

projects[migrate_extras][subdir] = "contrib"
projects[migrate_extras][version] = 2.5
projects[migrate_extras][patch][] = "https://drupal.org/files/migrate_extras-fix-destid2-array-1951904-4.patch"

projects[date][subdir] = "contrib"
projects[date][version] = 2.7

projects[yottaa][subdir] = "contrib"
projects[yottaa][version] = 1.2

projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = 1.0-rc2

projects[fences][subdir] = "contrib"
projects[fences][version] = "1.0"
projects[fences][patch][] = "https://drupal.org/files/undefined-index-1561244-7.patch"
projects[fences][patch][] = "https://drupal.org/files/fences-default_markup_option-1857230-2.patch"

projects[title][subdir] = "contrib"
projects[title][version] = "1.0-alpha7"
projects[title][patch][] = "https://drupal.org/files/title-translation_overwrite-1269076-35.patch"

projects[checklistapi][subdir] = "contrib"
projects[checklistapi][version] = 1.1

projects[context][subdir] = "contrib"
projects[context][version] = 3.2

; Search related modules.
projects[search_api][subdir] = "contrib"
projects[search_api][version] = 1.13

projects[search_api_db][subdir] = "contrib"
projects[search_api_db][version] = 1.3

projects[search_api_ranges][subdir] = "contrib"
projects[search_api_ranges][version] = 1.5
projects[search_api_ranges][patch][] = "https://www.drupal.org/files/issues/search_api_ranges-rewrite-data-alteration-callback-2001846-4.patch"

projects[facetapi][subdir] = "contrib"
projects[facetapi][version] = 1.5
projects[facetapi][patch][] = "https://drupal.org/files/facetapi-1616518-13-show-active-term.patch"

projects[search_api_sorts][subdir] = "contrib"
projects[search_api_sorts][version] = 1.5

projects[elasticsearch_connector][subdir] = "contrib"
projects[elasticsearch_connector][version] = 7.x-1.0-alpha3

; UI improvement modules.
projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = 2.0-alpha2

projects[image_delta_formatter][subdir] = "contrib"
projects[image_delta_formatter][version] = 1.0-rc1

projects[link][subdir] = "contrib"
projects[link][version] = 1.2
projects[link][patch][] = "https://drupal.org/files/Fixed_title_value_in_link_field_update_instance_undefined-1914286-3.patch"
projects[link][patch][] = "https://drupal.org/files/link-fix-undefined-index-widget-1918850-9.patch"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = 1.2

projects[cloud_zoom][subdir] = "contrib"
projects[cloud_zoom][version] = 1.x-dev

projects[special_menu_items][subdir] = "contrib"
projects[special_menu_items][version] = 2.0

projects[chosen][subdir] = "contrib"
projects[chosen][version] = 2.x-dev

projects[simplified_menu_admin][subdir] = "contrib"
projects[simplified_menu_admin][version] = 1.0-beta2

projects[navbar][subdir] = "contrib"
projects[navbar][version] = 1.4

projects[breakpoints][subdir] = contrib
projects[breakpoints][version] = 1.2

projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[json2][subdir] = contrib
projects[json2][version] = 1.1

; UI improvement themes.
projects[ember][subdir] = contrib
projects[ember][type] = theme
projects[ember][version] = 2.0-alpha2

projects[responsive_bartik][subdir] = contrib
projects[responsive_bartik][type] = theme
projects[responsive_bartik][version] = 1.0-rc1

projects[jquery_update][subdir] = contrib
projects[jquery_update][type] = module
projects[jquery_update][version] = 2.x-dev

projects[gridbuilder][subdir] = contrib
projects[gridbuilder][type] = module
projects[gridbuilder][version] = 1.0-alpha2

; SEO Modules
projects[seo_checklist][subdir] = "contrib"
projects[seo_checklist][version] = 4.1

projects[metatag][subdir] = "contrib"
projects[metatag][version] = 1.0-rc1

projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = 1.5

projects[redirect][subdir] = "contrib"
projects[redirect][version] = 1.0-rc1

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = 2.0

projects[google_tag][subdir] = "contrib"
projects[google_tag][version] = 1.0

projects[ga_tokenizer][subdir] = "contrib"
projects[ga_tokenizer][version] = 1.5

projects[contact_google_analytics][subdir] = "contrib"
projects[contact_google_analytics][version] = 1.4

projects[context_keywords][subdir] = "contrib"
projects[context_keywords][version] = 1.0-beta1

projects[microdata][subdir] = "contrib"
projects[microdata][version] = 1.0-beta2

projects[htmlpurifier][subdir] = "contrib"
projects[htmlpurifier][version] = 1.0

projects[search404][subdir] = "contrib"
projects[search404][version] = 1.3

projects[seo_checker][subdir] = "contrib"
projects[seo_checker][version] = 1.7

projects[linkchecker][subdir] = "contrib"
projects[linkchecker][version] = 1.2

projects[site_verify][subdir] = "contrib"
projects[site_verify][version] = 1.1

projects[security_review][subdir] = "contrib"
projects[security_review][version] = 1.1

; Performance modules
projects[coder][subdir] = "contrib"
projects[coder][version] = 2.2

projects[pasc][subdir] = "contrib"
projects[pasc][version] = 1.0-beta1

; Internationalization
projects[variable][subdir] = "contrib"
projects[variable][version] = 2.5

projects[i18n][subdir] = "contrib"
projects[i18n][version] = "1.x-dev"

projects[l10n_update][subdir] = "contrib"
projects[l10n_update][version] = 7.x-2.x-dev

projects[lingotek][subdir] = "contrib"
projects[lingotek][version] = 5.09

; Base theme.
projects[omega][version] = 4.2

projects[omega_kickstart][version] = 3.4

projects[shiny][version] = 1.5

; Libraries.
; NOTE: These need to be listed in https://drupal.org/packaging-whitelist.
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.3/ckeditor_4.4.3_full.zip"

libraries[json2][download][type] = get
libraries[json2][download][url] = "https://github.com/douglascrockford/JSON-js/archive/master.zip"
libraries[json2][revision] = fc535e9cc8fa78bbf45a85835c830e7f799a5084

libraries[underscore][download][type] = get
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.6.0.zip"

libraries[backbone][download][type] = get
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/1.1.2.zip"
