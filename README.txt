"Ready to GO" distribution have an idea to provide you with a great Drupal
distribution, containing all necessary modules for you to easily start using
Drupal with all the power it has!

Currently it is only an installation profile but the module is under active
development to become a real Drupal "Ready to GO" distribution. Currently it can
be used for a starting point for site builders.

Current features:
SEO - Providing all SEO modules technically required for a great distribution.

TODO:
Performance modules.
UI Modules for better user experience.
Interface tunings.
Installation of specific features.
Check link patches. check dev version modules and add revision.
